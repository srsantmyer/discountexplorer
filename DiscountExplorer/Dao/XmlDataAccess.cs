﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;

namespace DiscountExplorer.Dao
{
    public class XmlDataAccess
    {
        private static string _fileLocation = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/data.xml");

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet getData()
        {
            return getData(_fileLocation);
        }

        /// <summary>
        /// read the xml file from disk and return a dataset
        /// Note: adds dataset to cache and reads from cache on future calls
        /// Note: will need to update later when WRITES happen....
        /// </summary>
        /// <param name="fileLocation"></param>
        /// <returns>DataSet</returns>
        public static DataSet getData(string fileLocation)
        {
            DataSet ds = HttpRuntime.Cache["dataSet"] as DataSet;
            if(ds == null ) // there's no dataSet in the cache
            {
                ds = new DataSet();
                ds.ReadXml(fileLocation);
                HttpRuntime.Cache["dataSet"] = ds;
            }
            return ds;
        }
    }
}