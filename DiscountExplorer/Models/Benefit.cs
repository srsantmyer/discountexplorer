﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace DiscountExplorer.Models
{
    public class Benefit
    {
        /*
* <benefit id="0">
* <provider></provider>
* <vendor></vendor>
* <location></location>
* <offer></offer>
* <type></type>
* <value></value>
* <text></text>
* <limitations></limitations>
* <active></active>
* </benefit>
*/
        public int Id { get; set; }

        [Required]
        public string Provider { get; set;} 

        [Required]
        public string Vendor { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        public string Offer { get; set; }

        [Required]
        public string Value { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public string Limitations { get; set; }

        [Required]
        public bool Active { get; set; }

        /// <summary>
        /// generic constructor
        /// </summary>
        public Benefit()
        {
        }

        /// <summary>
        /// make a benefit from a data row
        /// </summary>
        /// <param name="dataRow"></param>
        internal static Benefit MapFromDataRow(DataRow dataRow)
        {
            Benefit r = new Benefit();

            bool b;
            int i;
            bool.TryParse(dataRow["active"].ToString(), out b);
            int.TryParse(dataRow["id"].ToString(), out i);
            r.Active = b;
            r.Id = i;
            r.Limitations = dataRow["limitations"].ToString();
            r.Location = dataRow["location"].ToString();
            r.Offer = dataRow["offer"].ToString();
            r.Provider = dataRow["provider"].ToString();
            r.Text = dataRow["text"].ToString();
            r.Value = dataRow["value"].ToString();
            r.Vendor = dataRow["vendor"].ToString();

            return r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataSet"></param>
        /// <returns></returns>
        public static List<Benefit> GetBenefits(DataSet dataSet)
        {
            List<Benefit> r = new List<Benefit>();

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                Benefit b = MapFromDataRow(row);
                r.Add(b);
            }

            return r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static List<Benefit> GetBenefits()
        {
            return GetBenefits(Dao.XmlDataAccess.getData());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static object GetBenefit(int id)
        {
            List<Benefit> benefits = GetBenefits();
            // var element = myList.Find(e => [some condition on e]);
            var r = benefits.Find(e => e.Id == id);
            return r;
        }
    }
}