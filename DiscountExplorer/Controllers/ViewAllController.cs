﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiscountExplorer.Controllers
{
    public class ViewAllController : Controller
    {
        // GET: ViewAll
        public ActionResult Index()
        {
            // return View();
            return View(Models.Benefit.GetBenefits());
        }

        // GET: ViewAll/Details/5
        public ActionResult Details(int id)
        {
            return View(Models.Benefit.GetBenefit(id));
        }

        // GET: ViewAll/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ViewAll/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ViewAll/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ViewAll/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ViewAll/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ViewAll/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
