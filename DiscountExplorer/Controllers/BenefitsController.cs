﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using DiscountExplorer.Models;
using Microsoft.Data.OData;

namespace DiscountExplorer.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DiscountExplorer.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Benefit>("Benefits");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class BenefitsController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/Benefits
        public async Task<IHttpActionResult> GetBenefits(ODataQueryOptions<Benefit> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            List<Benefit> benefits = Benefit.GetBenefits(Dao.XmlDataAccess.getData());

            return Ok<IEnumerable<Benefit>>(benefits);
            // return StatusCode(HttpStatusCode.NotImplemented);
        }

        // GET: odata/Benefits(5)
        public async Task<IHttpActionResult> GetBenefit([FromODataUri] int key, ODataQueryOptions<Benefit> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            // return Ok<Benefit>(benefit);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: odata/Benefits(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<Benefit> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Put(benefit);

            // TODO: Save the patched entity.

            // return Updated(benefit);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: odata/Benefits
        public async Task<IHttpActionResult> Post(Benefit benefit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            // return Created(benefit);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/Benefits(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Benefit> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Patch(benefit);

            // TODO: Save the patched entity.

            // return Updated(benefit);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/Benefits(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            // TODO: Add delete logic here.

            // return StatusCode(HttpStatusCode.NoContent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
