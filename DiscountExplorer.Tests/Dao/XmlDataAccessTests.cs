﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DiscountExplorer.Dao;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscountExplorer.Dao.Tests
{
    [TestClass()]
    public class XmlDataAccessTests
    {
        [TestMethod()]
        public void getDataTest()
        {
            Assert.IsTrue(XmlDataAccess.getData("c:\\temp\\data.xml").Tables.Count > 0);
        }
    }
}