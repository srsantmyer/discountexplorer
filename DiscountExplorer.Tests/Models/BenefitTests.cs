﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DiscountExplorer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscountExplorer.Models.Tests
{
    [TestClass()]
    public class BenefitTests
    {
        [TestMethod()]
        public void GetBenefitsTest()
        {
            Assert.IsTrue(Benefit.GetBenefits(Dao.XmlDataAccess.getData("c:\\temp\\data.xml")).Count > 0);
        }

        [TestMethod()]
        public void GetBenefitTest()
        {
            Assert.IsNotNull(Benefit.GetBenefit(1));
        }
    }
}